The Docker-pi: Docker Container Deployment in Fog Computing Infrastructures
================

## Principles

Docker-pi implements three optimization solutions to reduce Docker image deployment time. Docker-pi parallelizes all the hardware resources (download | CPU | extract) during an image deployment, therefore, it could improve the deployment time. The original work of Docker-pi has been published in IEEE EDGE 2019 and an exetended version in Inderscience Internation Journal of Cloud Computing.  For citation, 

```
Arif Ahmed, Guillaume Pierre. Docker Container Deployment in Fog Computing Infrastructures. IEEE EDGE 2018 - IEEE International Conference on Edge Computing, Jul 2018, San Francisco, CA, United States. pp.1-8, 
```

## How to compile Docker-pi?
1. Create a Docker build envrionment following the instructions give in the Docker website. Now insider the environment, perform following instructions.
2. Download pgzip Go package. For this run ./get-pgzip.sh
3. Now, compile and start Docker daemon. For this run ./compile.sh
4. After succefully compilation, the binary files are available in: /binary-daemon/docker
5. Finally, you can run Docker-pi in the local host machine.

